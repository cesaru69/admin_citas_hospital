package com.bezkoder.spring.datajpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bezkoder.spring.datajpa.model.Citas;

public interface CitasRepository  extends JpaRepository<Citas, Long> {

}
