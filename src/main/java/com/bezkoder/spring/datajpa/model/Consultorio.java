package com.bezkoder.spring.datajpa.model;

import javax.persistence.*;

@Entity
@Table(name = "Consultorio")
public class Consultorio {

	@Override
	public String toString() {
		return "Consultorio [idConsultorio=" + idConsultorio + ", noConsultorio=" + noConsultorio + ", piso=" + piso
				+ "]";
	}
	public Consultorio(long idConsultorio, long noConsultorio, long piso) {
		super();
		this.idConsultorio = idConsultorio;
		this.noConsultorio = noConsultorio;
		this.piso = piso;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idConsultorio;
	public long getIdConsultorio() {
		return idConsultorio;
	}
	public void setIdConsultorio(long idConsultorio) {
		this.idConsultorio = idConsultorio;
	}
	public long getNoConsultorio() {
		return noConsultorio;
	}
	public void setNoConsultorio(long noConsultorio) {
		this.noConsultorio = noConsultorio;
	}
	public long getPiso() {
		return piso;
	}
	public void setPiso(long piso) {
		this.piso = piso;
	}
	@Column(name = "NoConsultorio")
	private long noConsultorio;
	@Column(name = "Piso")
	private long piso;
}
