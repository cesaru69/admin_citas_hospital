package com.bezkoder.spring.datajpa.model;

import javax.persistence.*;


@Entity
@Table(name = "Doctor")
public class Doctor {
	
	@Override
	public String toString() {
		return "Doctor [idDoctor=" + idDoctor + ", nombre=" + nombre + ", apellidoPaterno=" + apellidoPaterno
				+ ", apellidoMaterno=" + apellidoMaterno + ", expecialidad=" + expecialidad + "]";
	}

	public Doctor(){
		
	}
	
	public Doctor(String nombre, String apellidoPaterno, String apellidoMaterno, String expecialidad) {
		super();
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.expecialidad = expecialidad;
	}
	
	public Doctor(long idDoctor, String nombre, String apellidoPaterno, String apellidoMaterno, String expecialidad) {
		super();
		this.idDoctor = idDoctor;
		this.nombre = nombre;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.expecialidad = expecialidad;
	}

	public long getIdDoctor() {
		return idDoctor;
	}
	public void setIdDoctor(long idDoctor) {
		this.idDoctor = idDoctor;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getExpecialidad() {
		return expecialidad;
	}
	public void setExpecialidad(String expecialidad) {
		this.expecialidad = expecialidad;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idDoctor;
	@Column(name = "Nombre")
	private String nombre;
	@Column(name = "ApellidoPaterno")
	private String apellidoPaterno;
	@Column(name = "ApellidoMaterno")
	private String apellidoMaterno;
	@Column(name = "Expecialidad")
	private String expecialidad;

}
