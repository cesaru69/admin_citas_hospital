package com.bezkoder.spring.datajpa.model;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Citas")
public class Citas {

	
	public Citas() {
		super();
	}

	@Override
	public String toString() {
		return "Citas [idCita=" + idCita + ", idConsultorio=" + idConsultorio + ", idDoctor=" + idDoctor
				+ ", horaConsulta=" + horaConsulta + ", nombrePaciente=" + nombrePaciente + "]";
	}

	public long getIdCita() {
		return idCita;
	}

	public void setIdCita(long idCita) {
		this.idCita = idCita;
	}

	public long getIdConsultorio() {
		return idConsultorio;
	}

	public void setIdConsultorio(long idConsultorio) {
		this.idConsultorio = idConsultorio;
	}

	public long getIdDoctor() {
		return idDoctor;
	}

	public void setIdDoctor(long idDoctor) {
		this.idDoctor = idDoctor;
	}

	public Date getHoraConsulta() {
		return horaConsulta;
	}

	public void setHoraConsulta(Date horaConsulta) {
		this.horaConsulta = horaConsulta;
	}

	public String getNombrePaciente() {
		return nombrePaciente;
	}

	public void setNombrePaciente(String nombrePaciente) {
		this.nombrePaciente = nombrePaciente;
	}

	public Citas(long idConsultorio, long idDoctor, Date horaConsulta, String nombrePaciente) {
		super();
		this.idConsultorio = idConsultorio;
		this.idDoctor = idDoctor;
		this.horaConsulta = horaConsulta;
		this.nombrePaciente = nombrePaciente;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idCita;
	
	@Column(name = "IdConsultorio")
	private long idConsultorio;
	
	@Column(name = "idDoctor")
	private long idDoctor;
	
	@Column(name = "HoraConsulta")
	private Date horaConsulta;
	
	@Column(name = "NombrePaciente")
	private String nombrePaciente;
}
